use cursive::Cursive;
use map::node::NodeType;
use cursive::views::Canvas;
use map::map::MapController;
use cursive::views::Dialog;
use cursive::views::TextView;
use cursive::utils::markup::StyledString;

pub struct Application {}

impl Application {
    pub fn select_new_node_type(curs: &mut Cursive, node_type: NodeType) {
        curs.call_on_id("app_canvas", |canvas: &mut Canvas<MapController>| {
            let mut state = canvas.state_mut();
            state.select_node_type(node_type);
        });
    }

    pub fn save_map(curs: &mut Cursive) {
        Application::show_dialog(curs, "Map saved !");
    }

    fn show_dialog<S>(curs: &mut Cursive, text: S)
        where S: Into<StyledString> {
        curs.add_layer(
            Dialog::around(
                TextView::new(text)
            ).dismiss_button("OK")
        )
    }

    pub fn clear_map(curs: &mut Cursive) {
        let res = curs.call_on_id("app_canvas", |canvas: &mut Canvas<MapController>| {
            let mut state = canvas.state_mut();
            state.clear_route()
        });
    }

    pub fn find_route(curs: &mut Cursive) {
        let res = curs.call_on_id("app_canvas", |canvas: &mut Canvas<MapController>| {
            let mut state = canvas.state_mut();
            state.try_find_route()
        });

        if let Some(res) = res {
            match res {
                Ok(_) => Application::show_dialog(curs, "Found a route !"),
                Err(e) => Application::show_dialog(curs, e),
            }
        }
    }
}