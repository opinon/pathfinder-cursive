pub enum NavigationType {
    Normal,
    Diagonal,
}

impl NavigationType {
    pub fn get_val(&self) -> u32 {
        match self {
            NavigationType::Normal => 1,
            NavigationType::Diagonal => 2,
        }
    }
}