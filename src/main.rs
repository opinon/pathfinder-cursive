extern crate cursive;
extern crate pathfinding;

mod map;
mod path;
mod app;

use cursive::Cursive;
use cursive::views::Canvas;
use cursive::traits::{Boxable, Identifiable};
use map::map::MapController;
use cursive::XY;
use cursive::menu::MenuTree;
use cursive::event::Key;

use app::app::Application;
use map::node::NodeType;

fn main() {

    // Loads cursive
    let mut curs: Cursive = Cursive::default();
    curs
        .load_theme_file("assets/default_theme.toml")
        .expect("Could not load theme file");

    // TODO: Loads a map from argument ?

    // By default, creates a new map with the size of the current terminal
    let screen_size = curs.screen_size();
    let canvas_size = XY::new(screen_size.x - 1, screen_size.y - 1);

    // Q exists the application, ESC brings up the menu
    curs.add_global_callback('q', Cursive::quit);
    curs.add_global_callback(Key::Esc, |cursive| cursive.select_menubar());

    // Adds the default layer
    curs.add_layer(
        Canvas::new(MapController::new(canvas_size.clone()))
            .with_draw(MapController::draw)
            .with_on_event(MapController::on_event)
            .with_id("app_canvas")
            .fixed_size(canvas_size));

    curs
        .menubar()
        .add_subtree("File",
                     MenuTree::new()
                         .leaf("New", move |curs| {})
                         .leaf("Open", move |curs| {})
                         .leaf("Save", move |curs| Application::save_map(curs)),
        )
        .add_subtree("Edit",
                     MenuTree::new()
                         .leaf("Void  .", move |curs| Application::select_new_node_type(curs, NodeType::Void))
                         .leaf("Wall  x", move |curs| Application::select_new_node_type(curs, NodeType::Wall))
                         .leaf("Start S", move |curs| Application::select_new_node_type(curs, NodeType::Start))
                         .leaf("End   E", move |curs| Application::select_new_node_type(curs, NodeType::End)),
        )
        .add_subtree("Find",
                     MenuTree::new()
                         .leaf("Find route", move |curs| Application::find_route(curs))
                         .leaf("Clear route", move |curs| Application::clear_map(curs)),
        )
        .add_leaf("Quit", Cursive::quit);

    // Runs the application
    curs.run();
}
