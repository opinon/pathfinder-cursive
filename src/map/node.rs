use cursive::Printer;
use cursive::Vec2;
use cursive::theme::ColorStyle;
use cursive::XY;
use path::navigation::NavigationType;

#[derive(PartialEq, Eq, Clone, Copy, Hash)]
pub enum NodeType {
    Void,
    Wall,
    Start,
    End,
    Travel,
}

impl NodeType {
    pub fn char_repr(&self) -> &str {
        match self {
            NodeType::Void => ".",
            NodeType::Wall => "x",
            NodeType::Start => "S",
            NodeType::End => "E",
            NodeType::Travel => "o",
        }
    }

    pub fn denom(&self) -> &str {
        match self {
            NodeType::Start => "start",
            NodeType::End => "end",
            _ => "",
        }
    }
}

#[derive(PartialEq, Eq, Clone, Hash)]
pub struct MapNode {
    x: u32,
    y: u32,
    data: NodeType,
}

impl MapNode {
    pub fn new(x: u32, y: u32, data: NodeType) -> Self {
        MapNode {
            x,
            y,
            data,
        }
    }

    pub fn draw(&self, printer: &Printer, cursor: &XY<u32>) {
        if cursor.x == self.x && cursor.y == self.y {
            printer.with_color(ColorStyle::highlight()
                               , |printer|
                                   printer.print((self.x, self.y), self.data.char_repr()),
            );
        } else {
            printer.print((self.x, self.y), self.data.char_repr());
        }
    }

    pub fn check_position(&self, pos: &XY<u32>) -> bool {
        pos.x == self.x && pos.y == self.y
    }

    pub fn set_type(&mut self, node_type: NodeType) {
        self.data = node_type;
    }

    pub fn data_match(&self, node_type: &NodeType) -> bool {
        self.data == *node_type
    }

    pub fn neighbors_pos(pos: &XY<u32>, map_size: &XY<u32>, move_type: NavigationType) -> Vec<XY<u32>> {
        let min_x = pos.x.checked_sub(1).unwrap_or(0);
        let max_x = match pos.x.checked_add(1) {
            Some(x) if x < map_size.x => x,
            _ => map_size.x - 1,
        };

        let min_y = pos.y.checked_sub(1).unwrap_or(0);
        let max_y = match pos.y.checked_add(1) {
            Some(y) if y < map_size.y => y,
            _ => map_size.y - 1,
        };


        (min_x..=max_x)
            .map(|x| (min_y..=max_y)
                .filter_map(|y| {
                    let delta = x.max(pos.x) - x.min(pos.x) + y.max(pos.y) - y.min(pos.y);
                    if !(x == pos.x && y == pos.y) && (delta <= move_type.get_val()) {
                        return Some(XY::new(x, y));
                    }
                    None
                })
                .collect())
            .flat_map(|i: Vec<XY<u32>>| i.into_iter())
            .collect()
    }

    pub fn walkable(&self) -> bool {
        match self.data {
            NodeType::Wall => false,
            _ => true,
        }
    }

    pub fn get_pos(&self) -> XY<u32> {
        XY::new(self.x, self.y)
    }

    pub fn distance(&self, other: &XY<u32>) -> u32 {
        let dx = self.x.max(other.x) - self.x.min(other.x);
        let dy = self.y.max(other.y) - self.y.min(other.y);
        dx + dy
    }

    pub fn start_or_end_point(&self) -> bool {
        self.data == NodeType::Start || self.data == NodeType::End
    }

    pub fn is_travel(&self) -> bool {
        self.data == NodeType::Travel
    }
}


#[cfg(test)]
pub mod tests {
    use map::node::MapNode;
    use map::node::NodeType;
    use cursive::XY;
    use path::navigation::NavigationType;

    #[test]
    fn test_neighbor_pos_normal() {
        let node: MapNode = MapNode::new(5, 5, NodeType::Void);
        assert_eq!(MapNode::neighbors_pos(&node.get_pos(), &XY::new(10, 10), NavigationType::Normal),
                   vec![
                       XY::new(4, 5),
                       XY::new(5, 4),
                       XY::new(5, 6),
                       XY::new(6, 5),
                   ]);
    }

    #[test]
    fn test_neighbor_pos_normal_limits() {
        // Left limit on X axis
        let node: MapNode = MapNode::new(0, 1, NodeType::Void);
        assert_eq!(MapNode::neighbors_pos(&node.get_pos(), &XY::new(10, 10), NavigationType::Normal),
                   vec![
                       XY::new(0, 0),
                       XY::new(0, 2),
                       XY::new(1, 1),
                   ]);

        // Right limit on X axis
        let node: MapNode = MapNode::new(9, 1, NodeType::Void);
        assert_eq!(MapNode::neighbors_pos(&node.get_pos(), &XY::new(10, 10), NavigationType::Normal),
                   vec![
                       XY::new(8, 1),
                       XY::new(9, 0),
                       XY::new(9, 2),
                   ]);
    }
}
