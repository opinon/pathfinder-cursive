use map::node::MapNode;
use cursive::Vec2;
use cursive::Printer;
use map::node::NodeType;
use cursive::XY;
use cursive::event::Event;
use cursive::event::EventResult;
use cursive::event::Key;
use std::error::Error;

use pathfinding::prelude::astar;
use path::navigation::NavigationType;

pub struct Map {
    nodes: Vec<MapNode>,
    map_size: XY<u32>,
}

pub struct MapController {
    map: Map,
    cursor: XY<u32>,
    selected_node_type: NodeType,
}

impl Map {
    pub fn new_empty(size_x: u32, size_y: u32) -> Self {
        let nodes: Vec<MapNode> =
            (0..size_x)
                .map(|x|
                    (0..size_y)
                        .map(|y| {
                            MapNode::new(x, y, NodeType::Void)
                        })
                        .collect()
                )
                .flat_map(|i: Vec<MapNode>| i.into_iter())
                .collect();
        Map {
            nodes,
            map_size: XY::new(size_x, size_y),
        }
    }

    pub fn draw(&self, printer: &Printer, cursor: &XY<u32>) {
        self
            .nodes
            .iter()
            .for_each(|n| {
                n.draw(printer, cursor);
            });
    }

    pub fn set_node(&mut self, pos: &XY<u32>, node_type: NodeType) {
        let mut node = self
            .nodes
            .iter_mut()
            .filter(|n| n.check_position(pos))
            .next();

        match node {
            Some(ref mut n) => n.set_type(node_type),
            _ => (),
        }
    }

    pub fn find_specific_point(&self, node_type: NodeType) -> Result<&MapNode, String> {
        self
            .nodes
            .iter()
            .filter(|n| n.data_match(&node_type))
            .next()
            .ok_or(format!("Could not find {} point", node_type.denom()))
    }

    pub fn node_walkable(&self, pos: &XY<u32>) -> bool {
        match self
            .nodes
            .iter()
            .filter(|n| n.check_position(pos))
            .next() {
            Some(n) => n.walkable(),
            _ => false,
        }
    }

    pub fn apply_path(&mut self, pos: Vec<XY<u32>>) {
        self
            .nodes
            .iter_mut()
            .filter(|n| pos.iter().filter(|p| n.check_position(p)).count() > 0 && !n.start_or_end_point())
            .for_each(|n| {
                n.set_type(NodeType::Travel)
            })
    }

    pub fn clear_travel(&mut self) {
        self
            .nodes
            .iter_mut()
            .filter(|n| n.is_travel())
            .for_each(|n| n.set_type(NodeType::Void))
    }
}

impl MapController {
    pub fn new(map_size: Vec2) -> Self {
        MapController {
            map: Map::new_empty(map_size.x as u32, map_size.y as u32),
            cursor: XY::new(0, 0),
            selected_node_type: NodeType::Wall,
        }
    }

    pub fn on_event(&mut self, event: Event) -> EventResult {
        match event {
            Event::Char(c) => self.handle_char(c),
            Event::Key(k) => self.handle_keys(k),
            _ => EventResult::Ignored
        }
    }

    pub fn handle_keys(&mut self, k: Key) -> EventResult {
        match k {
            Key::Left => {
                self.move_cursor(XY::new(-1, 0));
                EventResult::Consumed(None)
            }
            Key::Up => {
                self.move_cursor(XY::new(0, -1));
                EventResult::Consumed(None)
            }
            Key::Down => {
                self.move_cursor(XY::new(0, 1));
                EventResult::Consumed(None)
            }
            Key::Right => {
                self.move_cursor(XY::new(1, 0));
                EventResult::Consumed(None)
            }
            _ => EventResult::Ignored,
        }
    }

    pub fn try_find_route(&mut self) -> Result<(), String> {
        let result: Result<(Vec<XY<u32>>, u32), String> = {
            let start_point = self
                .map
                .find_specific_point(NodeType::Start)?;
            let end_point = self
                .map
                .find_specific_point(NodeType::End)?;

            let result: Option<(Vec<XY<u32>>, u32)> = astar(
                &start_point.get_pos(),
                |n|
                    MapNode::neighbors_pos(n, &self.map.map_size, NavigationType::Normal)
                        .into_iter()
                        .filter(|n| self.map.node_walkable(n))
                        .map(|n| (n, 1)),
                |n| end_point.distance(n),
                |n| end_point.distance(n) == 0,
            );
            result.ok_or("No path found !".into())
        };

        match result {
            Ok((path, cost)) => {
                self.map.apply_path(path);
                Ok(())
            }
            Err(e) => Err(e)
        }
    }

    pub fn handle_char(&mut self, c: char) -> EventResult {
        match c {
            'h' => {
                self.move_cursor(XY::new(-1, 0));
                EventResult::Consumed(None)
            }
            'j' => {
                self.move_cursor(XY::new(0, -1));
                EventResult::Consumed(None)
            }
            'k' => {
                self.move_cursor(XY::new(0, 1));
                EventResult::Consumed(None)
            }
            'l' => {
                self.move_cursor(XY::new(1, 0));
                EventResult::Consumed(None)
            }
            ' ' => {
                self.cursor_set_node();
                EventResult::Consumed(None)
            }
            _ => EventResult::Ignored,
        }
    }

    pub fn move_cursor(&mut self, dir: XY<i32>) {
        let new_x: Option<u32> = match dir.x {
            x if x < 0 => self.cursor.x.checked_sub(dir.x.abs() as u32),
            x if x > 0 => self.cursor.x.checked_add(dir.x.abs() as u32),
            _ => None,
        };

        let new_y: Option<u32> = match dir.y {
            y if y < 0 => self.cursor.y.checked_sub(dir.y.abs() as u32),
            y if y > 0 => self.cursor.y.checked_add(dir.y.abs() as u32),
            _ => None,
        };

        self.cursor.x = match new_x {
            Some(x) if x + 1 < self.map.map_size.x => x,
            _ => self.cursor.x,
        };

        self.cursor.y = match new_y {
            Some(y) if y + 1 < self.map.map_size.y => y,
            _ => self.cursor.y,
        };
    }

    pub fn clear_route(&mut self) {
        self.map.clear_travel();
    }

    pub fn cursor_set_node(&mut self) {
        self.map.set_node(&self.cursor, self.selected_node_type);
    }

    pub fn draw(&self, printer: &Printer) {
        self.map.draw(printer, &self.cursor);
    }

    pub fn select_node_type(&mut self, node_type: NodeType) {
        self.selected_node_type = node_type;
    }
}

pub struct MapView {
    controller: MapController,
    size: Vec2,
}