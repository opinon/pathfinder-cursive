# pathfinder-cursive

This is a small Rust project, port of my [plain ncurses C/Rust pathfinder](https://github.com/AlisCode/pathfinder-ncurses) using [Cursive](https://crates.io/crates/cursive) and [Pathfinding](https://crates.io/crates/pathfinding).

# Motivation

* Learning project 
* Testing Cursive (an awesome lib, really)
* Showcasing a small weekend project
